# Desired components

## Visualizations

### Word cloud

Show trending topics in a quick look

### Map of current stories

This can be done in two ways, what news sources based in that area say and locations that news articles are discussing. I think both could be useful.

### Person exploration graph

From the names extracted from news articles what are the topics around them? And what are the people connected to those topics? Contact chaining baby.

### Author graph

What topics are journo's discussing? How do they join up?

### Story spread mapping

Be able to show how an individual story has spread out over time. Be able to identify the first article and be able to make arbitrary animations showing it spreading between sources, locations and increasing intensity. Imagining a movie style disease spread animation.

### Categories

There needs to be some kind of broad categorical searching. Don't really have a clue on the best way to do this. I think fine-grain searching/filtering wouldn't go well though

### User profiles

Maybe an account feature that tailors results to user's likely interests based on past activities?

## Data Processing

### Batch Processing

There must be a way to process bulk data on a scheduled basis on AWS. I really can't find it though. This will be needed as data scales.

### Article Scraping

This will need a lot of attention, acquiring data is time and resource intensive. It's also the most likely thing in the processing chain to change unexpectedly. We have no control over how each news source handles presents it's data. I think the only way to approach this is a two pronged system of testing and monitoring. The article scraping code should be a private python package. Each news source would have it's own parsing class, the scraper as a whole should select the appropriate class based on a check of the url. Each datum extracted from the page should have it's own seperate function to handle just that single task. The entire thing needs extensive unit testing. This setup of individual allows immediate identification of the exact things going wrong. The python package as a whole should also have a separate testing module, which runs on a semi regular basis test a small sample for each data source against known good extractions. When running the module it needs to be handled such that exceptions are noticed and logged appropriately, with an alert raised when several errors for the same source. A backfill operation also needs to be made for fixing the inevitable breakages.

Literally everything else is based on this data so it needs to be done right and it needs to be done first.

### Social media

Could really use a source of social media sharing data for a metric of impact. Things like twitter and facebook shares per url. A quick look shows a few paid servies which claim to be able to do similar. Can a thing be hacked together for at least Twitter. TWINT?

### Sharing mechanisms

Modern web is sadly built around social media. We'll need a way of sharing things, which is difficult on purposefully ephemereal site. Maybe an export to image function for each visualization, could it be combined with base64 representation for immedaite posting to FaceBook/Twitter etc? Share query by url functionality, if a user has done some filtering and clicking thorugh subsections it should be possible to save/share this via a link. Like how a certain technological cook (CC) encodes recipes into the url.

## Data Science

### Keyword extraction

Need a good method of extracting keywords from text. This needs to be an unsupervised method to identify new things directly as they happen. Mathematical based techniques for this include tf-idf and TextRank. The benefit of textrank is that it doesn't require knowledge of any other article to work, which makes processing easy and a clearly unbiased selection.

I suspect a variant of topic modelling would work well. Typically topic modelling techniques tell you things are related by a shared topic but not what the topic is. I'm aware that at least one method to get a topic label exists. A bigger issue is I think topic modelling usually assigns one topic to each sample, it's likely an article will cover more than one topic.

Here's some promising-at-a-glance papers/articles covering existing work on story tracking/news article extraction:

https://arxiv.org/abs/1808.05906

https://www.wired.com/story/how-ai-tracking-coronavirus-outbreak/

https://medium.com/analytics-vidhya/automated-keyword-extraction-from-articles-using-nlp-bfd864f41b34

https://www.researchgate.net/publication/4374169_News_Keyword_Extraction_for_Topic_Tracking

### Text summarisation

Need some relatively cutting edge text summarisation. It needs to be able to take a set of arbitrary articles and combine then into a single paragraph. I've not heard of cross source sumamrisation like this before but it has heavy analogues with long form question-answering, e.g huggingface.co/qa. I heavily suspect this will require doing everything almost entirely from scratch. Here's one idea for how to approach this:
Some articles come with author-written summary, take one of those as a seed. Based on the story tracking/topic modelling technology find articles connected with the seed. Discard the article from the seed and just keep the summary, try to generate that target summary just from the other articles. This would likely be best done with some kind of transformer based architecture using transfer learning on one of the existing massive pre-trained models, liek BERT.

### Geographic extraction and graphing

Finding out what places are being discussed in an article is a non-trivial problem which I've not really explored, other than hearing about the modrecai package in a bad towardsdatascience article. I think this needs to be compbined with a knowledge graph so the article can be retrieved at whatever is the appropriate context. E.g. a mugging happens in Holsworthy. We want that article retrieved from a searches on England, Devon and Holsworthy but not from a search on Barnstable. Inversely a search on Devon should bring back a relavent incident in Exeter. Hierechical graph store.

### Time series analysis

Need to be able to categorise how virulent a story is. Predict eventual spread, metricate impact etc. Unusualness scores etc.

### Entity extraction

NLP techniques (nltk/SpaCy in particular) allow easy extraction of the entities in some text. Extract all person names discussed, places, companies etc