## Dynamodb

NoSQL database, part of the AWS free tier services. Very easy to control scaling. Given the current architecture this should be re-engineered. It is very important to get the indexing correct on initial setup. I've currently got it using the primary index of URL. I've yet to actually require searching by URL. I've needed to search by scrape status and publish date so far. I need to investigate how much cardinality effects indexing. Does the index need to be truly unique? Definitely need to get s3 file path attatched.


## S3

Fantastic, dead cheap storage. I'm currently storing all individuals individually in here. However that takes only a few kb per article. Maybe package things up by date published? Can article storage be done by compressing data and adding it to the database? Seems like the main limit is a max of 400kb per database item, articles can easily fit in that.


## Batch processing

Yet to figure out a good solution for this. I can't figure out how AWS Batch works and EMR seems to heavy duty and non-trivial to schedule. Currently using lambdas but I'm not sure how well this will scale. I'd prefer to keep everything serverless. I'll need to figure this out for anything large scale though.


## Article Scraping

Do as much processing as possible here. Need date, author, any tags, any summary, properly cleaned article text. Extract the keywords here as well.