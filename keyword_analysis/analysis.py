from summa import keywords
import boto3
import datetime
import gzip
import re
import hashlib
import json

from singular import singularize, stopwords

dynamo = boto3.resource('dynamodb')
table = dynamo.Table('meta')

s3 = boto3.resource('s3')

ALPHABET = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'

# Helper class to keep a running tally of the top urls for each keyword
class topContributors:
    def __init__(self, max_urls=5):
        self.pairs = []
        self.max_urls = max_urls
        
    def add(self, pair):
        self.pairs.append(pair)
        
        self.pairs = sorted(self.pairs, key=lambda x:x[1], reverse=True)
        
        if len(self.pairs) > self.max_urls:
            self.pairs = self.pairs[:5]
            
    def urls(self):
        return [x[0] for x in self.pairs]


# Very basic keyword detection
def get_keywords(article):
    # Remove some terms present in every article due to dodgy scraping
    article = article.replace('Image caption\n\n', '')
    article = re.sub(r'\n\nImage copyright\n.+?\n\n\n', '', article)

    # Collapse whitespace
    article = re.sub(r'\s+', ' ', article)
    article = article.strip()

    # Extract keywords, uses the summa package. Based on TextRank algorithm
    kws = keywords.keywords(article).split('\n')
    
    # Remove common english words and anything less than 3 characterise
    kws = [kw for kw in kws if kw not in stopwords and len(kw)>2]

    # Make plular forms of keywords single, cannabalised from the Pattern package
    kws = [singularize(kw) for kw in kws]
    kws = list(set(kws)) # Unique
    
    return kws


def get_file_name(url):
    def baseN(num,b,numerals="0123456789abcdefghijklmnopqrstuvwxyz"):
        return ((num == 0) and numerals[0]) or (baseN(num // b, b, numerals).lstrip(numerals[0]) + numerals[num % b])
    
    hex_data = hashlib.sha256(url.encode()).hexdigest() # Get the SHA2 hash of the url
    hex_data = hex_data[:16] # Take just the first quarter
    hex_data = int(hex_data, 16) # Convert into number form
    
    return baseN(hex_data, 62, ALPHABET)

def fetch_file(item):
    # Grab the article from s3
    pub = datetime.datetime.fromtimestamp(int(item['Published']))
    f_name = get_file_name(item['url'])
    
    obj = s3.Object('news-site-articles',f'{pub.year}/{pub.month}/{f_name}.txt.gz')
    body = obj.get()['Body'].read()
    article = gzip.decompress(body).decode() # Undo the space saving compression
    return article, pub


# Generate a mapping of key terms to importance scores
def create_full_term_scores(start_time, art_keywords, urls, dates, now):
    # Define a one week window. We will slide this window across a two week period to identify changes
    window_start = start_time
    window_end = window_start + 7*86400

    # Create a vocabulary containing every keyword seen
    vocab = set()
    for kw_set in art_keywords:
        for kw in kw_set:
            vocab.add(kw)

    day_map = {}
    top_kw_contributors = {}
    
    for i in range(7):
        # Slide the window
        scores = {}
        for kws, url, pub in zip(art_keywords, urls, dates):
            ts = pub.timestamp()
            if ts > window_start and ts < window_end:
                # Grab just the articles in the time window
                for kw in kws:
                    # the in-article score is a value between 0 and 1 based on an exponential time decay function.
                    # An article published now will give a score of 0, 14 days ago it will be 0.01

                    kw_score = 0.72**(((window_end-pub.timestamp()))/86400)
                    score = scores.get(kw, 0)
                    scores[kw] = kw_score+score # Create an overall score per keyword by adding all the per article scores

                    url_tracker = top_kw_contributors.get(kw, topContributors()) # Keep a record of the articles related to this keyword which contain the top 5 in article scores
                    url_tracker.add((url, score))
                    top_kw_contributors[kw] = url_tracker
        # Not every keyword is in every window, set these to zero
        unseen = vocab - set(scores.keys())
        for kw in unseen:
            scores[kw] = 0
        day_map[window_end] = scores

        window_start += 86400
        window_end += 86400

    return day_map, top_kw_contributors


def lambda_handler(event, context):
    # Article scores generated from a 2 week time slot
    now = datetime.datetime.now()
    start_time = now - datetime.timedelta(days=14)
    start_time = int(start_time.timestamp())
    
    # Grab the metadata from a dynamodb table.
    response = table.scan(
        FilterExpression=boto3.dynamodb.conditions.Attr('Published').gte(start_time) & boto3.dynamodb.conditions.Attr('scrapeStatus').not_exists(),
    )

    art_keywords = []
    dates = []
    urls = []
    titles = {}

    for item in response['Items']:
        # Download each article from s3
        article, pub = fetch_file(item)
        # Extract just the keywords
        art_keywords.append(get_keywords(article))

        # Record useful metadata for future (terrible way to do this, v. inefficient)
        dates.append(pub)
        urls.append(item['url'])
        titles[item['url']] = item['Title']

    
    # Create a map of importance scores for each keyword and the related article urls
    day_map, top_kw_contributors = create_full_term_scores(start_time, art_keywords, urls, dates, now)

    # Find the top 100 most recent keywords
    most_recent = max(day_map.keys())
    top100 = sorted(day_map[most_recent].items(), key=lambda x:x[1])[-100:]
    top100 = set([x[0] for x in top100])


    # Only retain the most important key terms
    reduced_day_map = {}
    for ts, term_map in day_map.items():
        reduced_day_map[ts] = {term:score for term,score in term_map.items() if term in top100}

    url_map = {kw:[(titles[url], url) for url in top_kw_contributors[kw].urls()] for kw in top100}

    # Bundle the data into a json file the fornt end can directly use
    output = {}
    output['score_map'] = reduced_day_map
    output['urls'] = url_map
    output = json.dumps(output).encode()
    
    # Dump this summary in the relevant s3 buckets for immediate use and archival
    object = s3.Object('news-site-summaries', 'latest.json')
    object.put(Body=output)

    object = s3.Object('news-site-front', 'latest.json')
    object.put(Body=output)

    object = s3.Object('news-site-summaries', now.isoformat()[:16].replace(':', '-')+'.json')
    object.put(Body=output)