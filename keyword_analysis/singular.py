import re

stopwords = set(["a","about","above","after","again","against","ain","all","am","an","and","any","are","aren",
             "aren't","as","at","be","because","been","before","being","below","between","both","but","by",
             "can","couldn","couldn't","d","did","didn","didn't","do","does","doesn","doesn't","doing","don",
             "don't","down","during","each","few","for","from","further","had","hadn","hadn't","has","hasn",
             "hasn't","have","haven","haven't","having","he","her","here","hers","herself","him","himself",
             "his","how","i","if","in","into","is","isn","isn't","it","it's","its","itself","just","ll","m",
             "ma","me","mightn","mightn't","more","most","mustn","mustn't","my","myself","needn","needn't","no"
             ,"nor","not","now","o","of","off","on","once","only","or","other","our","ours","ourselves","out",
             "over","own","re","s","same","shan","shan't","she","she's","should","should've","shouldn","shouldn't",
             "so","some","such","t","than","that","that'll","the","their","theirs","them","themselves","then","there",
             "these","they","this","those","through","to","too","under","until","up","ve","very","was","wasn",
             "wasn't","we","were","weren","weren't","what","when","where","which","while","who","whom","why","will",
             "with","won","won't","wouldn","wouldn't","y","you","you'd","you'll","you're","you've","your","yours",
             "yourself","yourselves","could","he'd","he'll","he's","here's","how's","i'd","i'll","i'm","i've",
             "let's","ought","she'd","she'll","that's","there's","they'd","they'll","they're","they've","we'd",
             "we'll","we're","we've","what's","when's","where's","who's","why's","would","able","abst","accordance",
             "according","accordingly","across","act","actually","added","adj","affected","affecting","affects",
             "afterwards","ah","almost","alone","along","already","also","although","always","among","amongst",
             "announce","another","anybody","anyhow","anymore","anyone","anything","anyway","anyways","anywhere",
             "apparently","approximately","arent","arise","around","aside","ask","asking","auth","available","away",
             "awfully","b","back","became","become","becomes","becoming","beforehand","begin","beginning","beginnings",
             "begins","behind","believe","beside","besides","beyond","biol","brief","briefly","c","ca","came","cannot",
             "can't","cause","causes","certain","certainly","co","com","come","comes","contain","containing","contains",
             "couldnt","date","different","done","downwards","due","e","ed","edu","effect","eg","eight","eighty","either",
             "else","elsewhere","end","ending","enough","especially","et","etc","even","ever","every","everybody",
             "everyone","everything","everywhere","ex","except","f","far","ff","fifth","first","five","fix","followed",
             "following","follows","former","formerly","forth","found","four","furthermore","g","gave","get","gets",
             "getting","give","given","gives","giving","go","goes","gone","got","gotten","h","happens","hardly","hed",
             "hence","hereafter","hereby","herein","heres","hereupon","hes","hi","hid","hither","home","howbeit",
             "however","hundred","id","ie","im","immediate","immediately","importance","important","inc","indeed",
             "index","information","instead","invention","inward","itd","it'll","j","k","keep","keeps","kept","kg","km",
             "know","known","knows","l","largely","last","lately","later","latter","latterly","least","less","lest","let",
             "lets","like","liked","likely","line","little","'ll","look","looking","looks","ltd","made","mainly","make",
             "makes","many","may","maybe","mean","means","meantime","meanwhile","merely","mg","might","million","miss",
             "ml","moreover","mostly","mr","mrs","much","mug","must","n","na","name","namely","nay","nd","near","nearly",
             "necessarily","necessary","need","needs","neither","never","nevertheless","new","next","nine","ninety",
             "nobody","non","none","nonetheless","noone","normally","nos","noted","nothing","nowhere","obtain","obtained",
             "obviously","often","oh","ok","okay","old","omitted","one","ones","onto","ord","others","otherwise","outside",
             "overall","owing","p","page","pages","part","particular","particularly","past","per","perhaps","placed",
             "please","plus","poorly","possible","possibly","potentially","pp","predominantly","present","previously",
             "primarily","probably","promptly","proud","provides","put","q","que","quickly","quite","qv","r","ran",
             "rather","rd","readily","really","recent","recently","ref","refs","regarding","regardless","regards",
             "related","relatively","research","respectively","resulted","resulting","results","right","run","said",
             "saw","say","saying","says","sec","section","see","seeing","seem","seemed","seeming","seems","seen","self",
             "selves","sent","seven","several","shall","shed","shes","show","showed","shown","showns","shows","significant",
             "significantly","similar","similarly","since","six","slightly","somebody","somehow","someone","somethan",
             "something","sometime","sometimes","somewhat","somewhere","soon","sorry","specifically","specified","specify",
             "specifying","still","stop","strongly","sub","substantially","successfully","sufficiently","suggest","sup",
             "sure","take","taken","taking","tell","tends","th","thank","thanks","thanx","thats","that've","thence",
             "thereafter","thereby","thered","therefore","therein","there'll","thereof","therere","theres","thereto",
             "thereupon","there've","theyd","theyre","think","thou","though","thoughh","thousand","throug","throughout",
             "thru","thus","til","tip","together","took","toward","towards","tried","tries","truly","try","trying","ts",
             "twice","two","u","un","unfortunately","unless","unlike","unlikely","unto","upon","ups","us","use","used",
             "useful","usefully","usefulness","uses","using","usually","v","value","various","'ve","via","viz","vol",
             "vols","vs","w","want","wants","wasnt","way","wed","welcome","went","werent","whatever","what'll","whats",
             "whence","whenever","whereafter","whereas","whereby","wherein","wheres","whereupon","wherever","whether",
             "whim","whither","whod","whoever","whole","who'll","whomever","whos","whose","widely","willing","wish",
             "within","without","wont","words","world","wouldnt","www","x","yes","yet","youd","youre","z","zero","a's",
             "ain't","allow","allows","apart","appear","appreciate","appropriate","associated","best","better","c'mon",
             "c's","cant","changes","clearly","concerning","consequently","consider","considering","corresponding","course",
             "currently","definitely","described","despite","entirely","exactly","example","going","greetings","hello","help",
             "hopefully","ignored","inasmuch","indicate","indicated","indicates","inner","insofar","it'd","keep","keeps","novel",
             "presumably","reasonably","second","secondly","sensible","serious","seriously","sure","t's","third","thorough",
             "thoroughly","three","well","wonder", 'media'])

singular_uninflected = set((
    "bison"      , "debris"   , "headquarters", "pincers"    , "trout"     ,
    "bream"      , "diabetes" , "herpes"      , "pliers"     , "tuna"      ,
    "breeches"   , "djinn"    , "high-jinks"  , "proceedings", "whiting"   ,
    "britches"   , "eland"    , "homework"    , "rabies"     , "wildebeest",
    "carp"       , "elk"      , "innings"     , "salmon"     ,
    "chassis"    , "flounder" , "jackanapes"  , "scissors"   ,
    "christmas"  , "gallows"  , "mackerel"    , "series"     ,
    "clippers"   , "georgia"  , "measles"     , "shears"     ,
    "cod"        , "graffiti" , "mews"        , "species"    ,
    "contretemps",              "mumps"       , "swine"      ,
    "corps"      ,              "news"        , "swiss"      ,
))
singular_uncountable = set((
    "advice"     , "equipment", "happiness"   , "luggage"    , "news"      , "software"     ,
    "bread"      , "fruit"    , "information" , "mathematics", "progress"  , "understanding",
    "butter"     , "furniture", "ketchup"     , "mayonnaise" , "research"  , "water"        ,
    "cheese"     , "garbage"  , "knowledge"   , "meat"       , "rice"      ,
    "electricity", "gravel"   , "love"        , "mustard"    , "sand"      ,
))
singular_ie = set((
    "alergie"    , "cutie"    , "hoagie"      , "newbie"     , "softie"    , "veggie"       ,
    "auntie"     , "doggie"   , "hottie"      , "nightie"    , "sortie"    , "weenie"       ,
    "beanie"     , "eyrie"    , "indie"       , "oldie"      , "stoolie"   , "yuppie"       ,
    "birdie"     , "freebie"  , "junkie"      , "^pie"       , "sweetie"   , "zombie"       ,
    "bogie"      , "goonie"   , "laddie"      , "pixie"      , "techie"    ,
    "bombie"     , "groupie"  , "laramie"     , "quickie"    , "^tie"      ,
    "collie"     , "hankie"   , "lingerie"    , "reverie"    , "toughie"   ,
    "cookie"     , "hippie"   , "meanie"      , "rookie"     , "valkyrie"  ,
))
singular_irregular = {
       "atlantes": "atlas",
        "atlases": "atlas",
           "axes": "axe",
         "beeves": "beef",
       "brethren": "brother",
       "children": "child",
        "corpora": "corpus",
       "corpuses": "corpus",
    "ephemerides": "ephemeris",
           "feet": "foot",
        "ganglia": "ganglion",
          "geese": "goose",
         "genera": "genus",
          "genii": "genie",
       "graffiti": "graffito",
         "helves": "helve",
           "kine": "cow",
         "leaves": "leaf",
         "loaves": "loaf",
            "men": "man",
      "mongooses": "mongoose",
         "monies": "money",
          "moves": "move",
         "mythoi": "mythos",
         "numena": "numen",
       "occipita": "occiput",
      "octopodes": "octopus",
          "opera": "opus",
         "opuses": "opus",
            "our": "my",
           "oxen": "ox",
          "penes": "penis",
        "penises": "penis",
         "people": "person",
          "sexes": "sex",
    "soliloquies": "soliloquy",
          "teeth": "tooth",
         "testes": "testis",
        "trilbys": "trilby",
         "turves": "turf",
            "zoa": "zoon",
}

singular_rules = [
    (r'(?i)(.)ae$'            , '\\1a'    ),
    (r'(?i)(.)itis$'          , '\\1itis' ),
    (r'(?i)(.)eaux$'          , '\\1eau'  ),
    (r'(?i)(quiz)zes$'        , '\\1'     ),
    (r'(?i)(matr)ices$'       , '\\1ix'   ),
    (r'(?i)(ap|vert|ind)ices$', '\\1ex'   ),
    (r'(?i)^(ox)en'           , '\\1'     ),
    (r'(?i)(alias|status)es$' , '\\1'     ),
    (r'(?i)([octop|vir])i$'   , '\\1us'  ),
    (r'(?i)(cris|ax|test)es$' , '\\1is'   ),
    (r'(?i)(shoe)s$'          , '\\1'     ),
    (r'(?i)(o)es$'            , '\\1'     ),
    (r'(?i)(bus)es$'          , '\\1'     ),
    (r'(?i)([m|l])ice$'       , '\\1ouse' ),
    (r'(?i)(x|ch|ss|sh)es$'   , '\\1'     ),
    (r'(?i)(m)ovies$'         , '\\1ovie' ),
    (r'(?i)(.)ombies$'        , '\\1ombie'),
    (r'(?i)(s)eries$'         , '\\1eries'),
    (r'(?i)([^aeiouy]|qu)ies$', '\\1y'    ),
        # -f, -fe sometimes take -ves in the plural
        # (e.g., lives, wolves).
    (r"([aeo]l)ves$"          , "\\1f"    ),
    (r"([^d]ea)ves$"          , "\\1f"    ),
    (r"arves$"                , "arf"     ),
    (r"erves$"                , "erve"    ),
    (r"([nlw]i)ves$"          , "\\1fe"   ),
    (r'(?i)([lr])ves$'        , '\\1f'    ),
    (r"([aeo])ves$"           , "\\1ve"   ),
    (r'(?i)(sive)s$'          , '\\1'     ),
    (r'(?i)(tive)s$'          , '\\1'     ),
    (r'(?i)(hive)s$'          , '\\1'     ),
    (r'(?i)([^f])ves$'        , '\\1fe'   ),
    # -ses suffixes.
    (r'(?i)(^analy)ses$'      , '\\1sis'  ),
    (r'(?i)((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$', '\\1\\2sis'),
    (r'(?i)(.)opses$'         , '\\1opsis'),
    (r'(?i)(.)yses$'          , '\\1ysis' ),
    (r'(?i)(h|d|r|o|n|b|cl|p)oses$', '\\1ose'),
    (r'(?i)(fruct|gluc|galact|lact|ket|malt|rib|sacchar|cellul)ose$', '\\1ose'),
    (r'(?i)(.)oses$'          , '\\1osis' ),
    # -a
    (r'(?i)([ti])a$'          , '\\1um'   ),
    (r'(?i)(n)ews$'           , '\\1ews'  ),
    (r'(?i)s$'                , ''        ),
]

# For performance, compile the regular expressions only once:
singular_rules = [(re.compile(r[0]), r[1]) for r in singular_rules]

def singularize(word):
    """ Returns the singular of a given word.
    """

    # Recurse compound words (e.g. mothers-in-law).
    if "-" in word:
        w = word.split("-")
        if len(w) > 1 and w[1] in plural_prepositions:
            return singularize(w[0], pos, custom) + "-" + "-".join(w[1:])
    # dogs' => dog's
    if word.endswith("'"):
        return singularize(word[:-1]) + "'s"
    w = word.lower()
    for x in singular_uninflected:
        if x.endswith(w):
            return word
    for x in singular_uncountable:
        if x.endswith(w):
            return word
    for x in singular_ie:
        if w.endswith(x + "s"):
            return w
    for x in singular_irregular:
        if w.endswith(x):
            return re.sub('(?i)' + x + '$', singular_irregular[x], word)
    for suffix, inflection in singular_rules:
        m = suffix.search(word)
        g = m and m.groups() or []
        if m:
            for k in range(len(g)):
                if g[k] is None:
                    inflection = inflection.replace('\\' + str(k + 1), '')
            return suffix.sub(inflection, word)
    return word