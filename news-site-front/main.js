var app = new Vue({
    el: '#app',
    data: {
        blob: null,
        urlBlob: null,
        selected: null,
        trendColour: false,
    },
    mounted() {
        // As soon as the page loads retrieve the most recent key word data
        this.downloadData();
    },
    methods: {
        downloadData: function(){
            var vm = this;
            d3.json('./latest.json').then(function(x){
                vm.blob = x.score_map;
                vm.urlBlob = x.urls;
            }).then(function(){
                vm.makeCloud();
                vm.selected = vm.topWords[0][0];
            })
        },
        toggleColouring: function(){
            this.trendColour = !this.trendColour;
            this.makeCloud();
        },
        makeCloud: function(){
            // set the dimensions and margins of the graph
            document.getElementById("wordCloud").innerHTML = ''

            var margin = {top: 10, right: 10, bottom: 10, left: 10},
            width = 1200 - margin.left - margin.right,
            height = 400 - margin.top - margin.bottom;

            // append the svg object to the body of the page
            this.svg = d3.select("#wordCloud").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform",
                "translate(" + margin.left + "," + margin.top + ")");

            // Constructs a new cloud layout instance. It run an algorithm to find the position of words that suits your requirements
            // Wordcloud features that are different from one word to the other must be here
            this.words = this.topWords.map(function(d) { return {word: d[0].toUpperCase(),
                size:+d[1],
                trend:+d[2]
                } })
            this.layout = d3.layout.cloud()
            .size([width, height])
            .words(this.words.map(function(d) { return {text: d.word, size:d.size, trend:d.trend}; }))
            .padding(1)        //space between words
            .rotate(0)
            .fontSize(function(d) { return d.size; })      // font size of words
            .on("end", this.draw);
            this.layout.start();
        },
        draw: function(words) {
            let fill = d3.scaleOrdinal(d3.schemeCategory10);
            let trendScaler = d3.interpolateInferno;
            let vm = this;
            this.svg.append("g")
                .attr("transform", "translate(" + this.layout.size()[0] / 2 + "," + this.layout.size()[1] / 2 + ")")
                .selectAll("text")
                .data(words)
                .enter().append("text")
                .style("font-size", function(d) { return d.size+'px'; })
                .style("fill", function(d){
                    if(vm.trendColour){
                        return trendScaler(d.trend)
                    }
                    else{
                        return fill(d.text.toLowerCase())
                    }
                })
                .attr("text-anchor", "middle")
                .style("font-family", "Impact")
                .attr("transform", function(d) {
                    return "translate(" + [d.x, d.y] + ")rotate(0)";
                })
                .text(function(d) { return d.text; })
                .on('click', function(d){
                    vm.selected = d.text.toLowerCase();
                })
                ;
          }
    },
    watch: {
        selected: function(val){
            /*
            * When the selected keyword changes redraw the trending ness line chart
            */
            // set the dimensions and margins of the graph
            var margin = {top: 10, right: 30, bottom: 30, left: 60},
            width = 900 - margin.left - margin.right,
            height = 350 - margin.top - margin.bottom;

            // append the svg object to the body of the page
            document.getElementById("lineChart").innerHTML = ''

            var svg = d3.select("#lineChart")
            .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform",
                "translate(" + margin.left + "," + margin.top + ")");


            let data = []
            for(let k of _.keys(this.blob)){
                data.push({
                    date: d3.timeParse("%s")(k),
                    value: this.blob[k][this.selected]
                })
            }

            // Add X axis --> it is a date format
            var x = d3.scaleTime()
            .domain(d3.extent(data, function(d) { return d.date; }))
            .range([ 0, width ]);
            svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x).tickFormat(d3.timeFormat("%d-%b")));

            // Add Y axis
            var y = d3.scaleLinear()
            .domain([0, d3.max(data, function(d) { return +d.value; })])
            .range([ height, 0 ]);
            svg.append("g")
            .call(d3.axisLeft(y));

            // Add the line
            svg.append("path")
            .datum(data)
            .attr("fill", "none")
            .attr("stroke", "steelblue")
            .attr("stroke-width", 1.5)
            .attr("d", d3.line()
                .x(function(d) { return x(d.date) })
                .y(function(d) { return y(d.value) })
                )

            //})
        }
    },
    computed: {
        latest: function(){
            if(this.blob){
                let lastKey = _.max(_.keys(this.blob));
                return this.blob[lastKey];
            }
            else{
                return null;
            }
        },
        topWords: function(){
            /*
                Identify the top 100 keywords and decide a fontsize based on the relative keyword scores.

                The part to find the top 100 can be removed now
            */
            if(this.blob){
                let sorted = _(this.latest)
                                .toPairs()
                                .orderBy([1], ['desc'])
                                .fromPairs()
                                .value();
                let sortedKeys = _.keys(sorted).slice(0,100);

                let previous = _.keys(this.blob).sort().reverse()[1];
                let previousData = this.blob[previous];

                sortedKeys =  sortedKeys.map(function(x){
                    return [x, sorted[x], sorted[x]-previousData[x]]; // Get key word, current score, diff form previous score
                });

                let maxVal = sortedKeys[0][1],
                    minVal = sortedKeys[99][1];
                let scaler = d3.scaleLinear()
                                .domain([minVal, maxVal])
                                .range([10,120]);

                let diffVals = sortedKeys.map(function(x){return x[2]});
                maxVal = _.max(diffVals);
                minVal = Math.abs(_.min(diffVals));
                let extremis = Math.max(minVal, maxVal);            

                return sortedKeys.map(function(x){
                    x[1] = scaler(x[1]);
                    x[2] = x[2]/extremis; // Scale between -1 and 1
                    x[2] = (x[2]+1)/2 // Scale between 0 and 1
                    return x
                })
            }
            else{
                return null;
            }
        },
        currentUrls: function() {
            /*
                Format the most relevant urls to the currently selected keyword
            */
            if(Boolean(this.urlBlob) & Boolean(this.selected)){
                return this.urlBlob[this.selected].map(function(x){
                    return {url: x[1], title:x[0]};
                });
            }
            else {
                return [];
            }
        },
        selectedWord: function(){
            if(this.selected){
                return this.selected.toUpperCase();
            }
            else{
                return undefined;
            }
        }
    }
})