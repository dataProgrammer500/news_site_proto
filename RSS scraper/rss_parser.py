import feedparser
import boto3
from botocore.exceptions import ClientError
import datetime


def lambda_handler(event, context):
    # Retrieve list of RSS feeds to check
    s3 = boto3.resource('s3')
    obj = s3.Object('news-site-backend-resources','rss_feeds.txt')
    body = obj.get()['Body'].read()
    
    rss_feeds = [x.split('|') for x in body.decode().split('\n')]

    # Create database connection
    dynamodb = boto3.resource('dynamodb', region_name='eu-west-2')
    table = dynamodb.Table('meta')

    # Get this using multithreading
    for source, feed in rss_feeds:
        print(source)
        feed = feedparser.parse(feed)
        for entry in feed.entries:
            # This needs to be customized for each news source. Will only work for BBC right now.
            try:
                title = entry.get('title')
                url = entry.links[0].href
                summary = entry.get('summary')

                published = entry.get('published')
                published = datetime.datetime.strptime(published, '%a, %d %b %Y %H:%M:%S %Z')
                published = published.timestamp()
            except Exception as e:
                print(e)
                continue
            
            # Shove extracted metadata in a NoSQL database
            try:
                table.put_item(Item={"Title": title,
                                     "Source": source,
                                     "url": url,
                                     "Summary": summary,
                                     "Published": int(published),
                                     "scrapeStatus": "False"
                                    },ConditionExpression="attribute_not_exists(Title)")
            except ClientError as e:  
                if e.response['Error']['Code']=='ConditionalCheckFailedException':  
                    continue
                else:
                    raise e