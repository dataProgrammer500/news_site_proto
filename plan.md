# Plan

Create a website for aggregate visualisations of current new stories. This will be done by developing a feed of near real time data from a wide variety of news sources. This data will be used directly to show current issues and stored over time for contextual analytics. This will enable quick overviews of recent news, identification of trending topics as they happen and easy viewing across range of sources. The target audience should be young professionals who want a quick view of what's going on but the option to go deeper.

The website front end shall be client side javascript which reads regularly generated files. The backend of the website should be based on serverless technologies and have no direct connection to the front, this allows for massive, cheap scalability as the only variable factor is file retrieval, not compute or database access like most systems. Purely static files also allow near instant updates for users exploring and the ability to distribute across CDNs for excellent speed. This is also a far more secure framework, can't hack a site which doesn't handle user inputs.

User experience should take precedence over in-depth exploration and data density, but the option to pursue should still exist. Exploring the interface should be intuitive, if a explanatory words are required then something's wrong. We're not going to try to pander to the lowest common denominator. Cater for the lazy smart person, not idiots. Desktop use should be the primary focus but a mobile form is also neccesary in this day and age. I suspect to keep appropriate information density these will need to be almost wholly different implementations.

## Phased development

This is a pretty massive project, waypoints, goals and progressive motivational payoff would be needed.

1) Data acquisition
The most important part of this project is the dataset, nothing can even get started without it. Scrapers are also notoriously fragile so it well need to be well engineered. Create the near real time feed for a group of the most prominent news organisations. Backfill a decent set of historic data, at least a couple of months.

2) Core algorithm development
Topic modelling, keyword extraction, story tracking, trend identification etc. all need to be figured out and well implemented.

3) MVP front end
Create a minimum viable product consisting of the general layout, core functionality,a few key visualisations, and smooth UX. Properly TLS enabled. Also require user logging at this point. Likely google analytics to take advantage of their existing ancilliary data.

4) Initial deployment
Website release. Will need a full name we're happy with. Share via profiles on twitter and reddit. Post cool bits on /r/dataisbeautiful and tweet about relevant hot button issues. Still keep individual anonymity. Also require an active reddit account to actually handle the posting, they only allow 10% self promotion, a literal excuse to shitpost.

5) Feature development
Expand to a larger set of news sources. Add new visualizations. Get started on the really hard analytics.

## Technologies

* AWS for the platform
* Python for core data processing
* HTML/CSS/JS for front end
* Vue.js for core framework
* Bootstrap for CSS prettifying
* D3 for visualizations

## Risks

Copyright, need to be careful about this. Each news source will be different. The key risk is when presenting scraped material back to users. Extracted keywords should be fine but there's a sliding scale after that towards what more litagious companies may consider reproduction. It's also likely that some companies may have specific clauses about what the material can be used for. As long has there's no attempt to pass of other's work as our own and drive traffic back towards the original sources then should be fine. I reckon scrape everything, even through paywalls, but only direct to open sources in the output.

Web scraping has historically been a bit of a legal grey area. There's not really any purposely made law covering web scraping and the case law is very undeveloped. All existing cases have been around damage towards a company being scraped, either by intensive resource use or trying to copy a proprietary database. This project doesn't do either of those, though. For each individual source a scraper is no more intensive than an enthusiastic reader maximum of 10s of requests a day. The value of the site comes from creating a database across a wide range of sources, not copying one private source in particular. Neither does it steal traffic from any individual site we use as a source. Worth keeping an eye on the robots.txt and T&Cs though.

Doing this will cost time and money. There's no avoiding this but it's a matter of degrees. My prototype running for a week has cost around $0.03 so far for one news source. The majority of that has come from placing objects in s3, the re-design requires far, far less object storage. Timewise is an opportunity cost situation, I don't expect a huge amount of maintenance will be required once development is complete (just keeping an eye on scrape error logs really) but I suspect over 100 hours of dev time will be required for a properly fleshed out result. This can't be done in working from home time due to employer copyright issues. So it would require a significant amount of personal free time.

## Monetisation

I don't like to start a large project unless there's at least some possibility of money to be made. For this project I see two main possibilities. The first is advertising. The scrupulous version would be a standard banner advert, perhaps adwords. The less scrupulous version would be merge adverts with the main data, along the lines of promoted links/stories. Advertising has the advantage of being easy to implement and a very well established way of earning money online. However it feels scummy to me and likely won't go well with the target market.

The second approach would be to provide an enhanced service to paying customers. This could involve far more in depth queries requiring individual compute that would be prohibitively expensive en masse. It could also involve a bespoke monitoring and alerting service for things like company media profile monitoring. Or more general OSINT.

Not even worth considering until 1000s of active, repeat users though.
