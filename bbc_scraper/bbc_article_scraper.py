import hashlib, gzip, datetime
import requests
import boto3
from bs4 import BeautifulSoup

ALPHABET = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'

def get_file_name(url):
    def baseN(num,b,numerals="0123456789abcdefghijklmnopqrstuvwxyz"):
        return ((num == 0) and numerals[0]) or (baseN(num // b, b, numerals).lstrip(numerals[0]) + numerals[num % b])
    
    hex_data = hashlib.sha256(url.encode()).hexdigest() # Get the SHA2 hash of the url
    hex_data = hex_data[:16] # Take just the first quarter
    hex_data = int(hex_data, 16) # Convert into number form
    
    return baseN(hex_data, 62, ALPHABET)
    

def parse_article(url):
    # Extract the article text from the raw html
    resp = requests.get(url)
    soup = BeautifulSoup(resp.text, 'html.parser')
    story = soup.find('div', {'class': 'story-body__inner'})
    return story.text


def lambda_handler(event, context):
    # Get urls from dynamodb
    dynamo = boto3.resource('dynamodb')
    table = dynamo.Table('meta')
    resp = table.query(
        IndexName='scrapeStatus-index',
        KeyConditionExpression=boto3.dynamodb.conditions.Key('scrapeStatus').eq("False"),
    )

    # Extract the urls to scrape, ignoring those with repeated failures
    urls = []
    for item in resp['Items']:
        if item.get('fails', 0)<3:
            urls.append((item['url'], item['Published']))

    s3 = boto3.resource('s3')

    for url, pub in urls:
        try:
            article = parse_article(url)
        except Exception as e:
            # If parsing fails print out the url and reason so they can be picked up by cloud watch
            print(url)
            print(e)
            # Increment the failure count in the metadata database
            response = table.update_item(
                Key={
                    'url': url,
                },
                UpdateExpression="SET fails = if_not_exists(fails, :start) + :inc",
                ExpressionAttributeValues={
                    ':inc': 1,
                    ':start': 0,
                },
            )
            continue

        # Generate the s3 bucket location
        published = datetime.datetime.fromtimestamp(int(pub))
        filename = get_file_name(url)
        filename = f'{published.year}/{published.month}/{filename}.txt.gz'

        # Compress the file for space saving and dump into s3
        compressed = gzip.compress(article.encode())

        object = s3.Object('news-site-articles', filename)
        object.put(Body=compressed)

        response = table.update_item(
            Key={
                'url': url,
            },
            UpdateExpression="remove scrapeStatus",
        )